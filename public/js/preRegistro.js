/// Funciones ///

/// Ready ///
var objReq = [];
var institucion;

$(document).ready( function () {
    
    $('#selectTipoParti').selectpicker();
    $('#selectGrado').selectpicker();
    $('#selectPtl').selectpicker();

    $('#selectPtl').change(function(){
        $('#labelOtra').hide();
        $('#plantel').hide();
        $('#plantel').val('');
        var inst = $(this).val();
        console.log(inst);
        if(inst === 'otra'){
            $('#labelOtra').show();
            $('#plantel').show();
            //institucion = $('#otraInst').val();
        }
        else{
            var nomPtl = $('#selectPtl option:selected').text();
            $('#plantel').val(nomPtl);
            //institucion = $(this).val();
        }              
    });

    $('#selectGrado').change(function(){
        $('#otroGrado').hide();
        $('#grado').hide();
        $('#grado').val('');
        var gdo = $(this).val();
        console.log(gdo);
        if(gdo === '1'){
            $('#otroGrado').show();
            $('#grado').show();
        }
        else{
            var grado = $('#selectGrado option:selected').text();
            $('#grado').val(grado);
            //institucion = $(this).val();
        }
    });
    

    
    
})//Ready