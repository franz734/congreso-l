<?php
/// Consultas ///
function getTipos(){
    $tipos = DB::select("select * from ct_participante order by tipo_parti");
    return $tipos;
}

function getGrados(){
    $grados = DB::select("select * from ct_grado order by id_grado");
    return $grados;    
}

function getEscuelas(){
    $escuelas = DB::connection('sqlsrvPTL')->select(" SELECT 
	P.ptl_ptl, P.ptl_nombre, case when char_length(STUFF(P.ptl_nombre , charindex('ESC ',P.ptl_nombre )  , 3 ,'ESCUELA' )) > 0 then STUFF(P.ptl_nombre , charindex('ESC ',P.ptl_nombre )  , 3 ,'ESCUELA' )
                    else
                          case when char_length(STUFF(P.ptl_nombre , charindex('INST ',P.ptl_nombre )  , 4 ,'INSTITUTO' )) > 0 then STUFF(P.ptl_nombre , charindex('INST ',P.ptl_nombre )  , 4,'INSTITUTO' )
                          else
                               case when char_length(STUFF(P.ptl_nombre , charindex('UNIV ',P.ptl_nombre )  , 4 ,'UNIVERSIDAD' )) > 0 then STUFF(P.ptl_nombre , charindex('UNIV ',P.ptl_nombre )  , 4,'UNIVERSIDAD' )
                               else 
                                 case when char_length(STUFF(P.ptl_nombre , charindex('COL ',P.ptl_nombre )  , 3 ,'COLEGIO' )) > 0 then STUFF(P.ptl_nombre , charindex('COL ',P.ptl_nombre )  , 3,'COLEGIO' )
                                 else
                                   case when char_length(STUFF(P.ptl_nombre , charindex('PREP ',P.ptl_nombre )  , 4 ,'PREPARATORIA' )) > 0 then STUFF(P.ptl_nombre , charindex('PREP ',P.ptl_nombre )  , 4,'PREPARATORIA' )
                                   else 
                                     case when char_length(STUFF(P.ptl_nombre , charindex('PREP ',P.ptl_nombre )  , 4 ,'PREPARATORIA' )) > 0 then STUFF(P.ptl_nombre , charindex('PREP ',P.ptl_nombre )  , 4,'PREPARATORIA' )
                                     else 
                                             case when char_length(STUFF(P.ptl_nombre , charindex('CEN ',P.ptl_nombre )  , 3 ,'CENTRO' )) > 0 then STUFF(P.ptl_nombre , charindex('CEN ',P.ptl_nombre )  , 3,'CENTRO' )
                                              else P.ptl_nombre 
                                          end
                                     end
                                   end
                                 end
                               end
                          end
                    end
    as nombre
    FROM unamsi.dbo.planteles P
    WHERE P.ptl_vig = 'S' and (P.ptl_nivel='P' or P.ptl_nivel='C' or P.ptl_nivel='L')  ORDER BY P.ptl_ptl ASC
    ");
    return $escuelas;
}
?>