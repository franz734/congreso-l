<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/// Registro ///
Route::get('pre-registro',function(){
    $tipo = getTipos();
    $grado = getGrados();
    $escuelas = getEscuelas();
    return view('registro.pre-registro',['tipo' => $tipo, 'grado' => $grado, 'escuelas' => $escuelas]);
});
Route::get('pre-registro2','PreRegistroController@panelPreReg');

/// pbs ///
/* Route::get('pbs', function(){
    $escuelas = getEscuelas();
    dd($escuelas);
}); */
/* Route::any('captcha-test', function() {
    if (request()->getMethod() == 'POST') {
        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            echo '<p style="color: #ff0000;">Incorrecto!</p>';
        } else {
            echo '<p style="color: #00ff30;">Correcto :)</p>';
        }
    }

    $form = '<form method="post" action="captcha-test">';
    $form .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
    $form .= '<p>' . captcha_img() . '</p>';
    $form .= '<p><input type="text" name="captcha"></p>';
    $form .= '<p><button type="submit" name="check">Check</button></p>';
    $form .= '</form>';
    return $form;
}); */
