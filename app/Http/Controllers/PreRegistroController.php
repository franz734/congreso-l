<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PreRegistroController extends Controller
{
    function panelPreReg(){
        $tipo = getTipos();
        $grado = getGrados();
        $escuelas = getEscuelas();
        return view('registro.pre-registro2',['tipo' => $tipo, 'grado' => $grado, 'escuelas' => $escuelas]);
    }
}
