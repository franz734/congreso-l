@extends('master2')
@section('contenido')
<div class="ui-state-default ui-corner-top" colspan="2" style="height:30px; padding-left:5px;" align="center">Pre - Registro a congreso</div>
    {{-- <div align="center" style="padding-top:35px; padding-bottom:15px;"> --}}                    
        <div class="component-section no-code">  
        <div class="ui-widget-header2 ui-corner-top ui-corner-bottom" align="center" style="padding:5px 0 5px 0; width:90%; margin:0 auto 0 auto; font-size:16px; margin-bottom:10px;">Datos generales</div>                          
            <div class="card card-body pd-lg-25">                
                <form id="preRegForm">
                    <div class="row">
                        <div class="col-12 col-md-3"><!--Etiquetas-->
                            <div class="row align-items-center justify-content-end  customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Tipo de Participante:</span></label> 
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label "<span style="color:black;font-weight:bold">Nombre:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Grado de Estudios:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel" style="display:none" id="otroGrado">
                                <label class="form-control-label"><span style="color:black;font-weight:bold"> Otro Grado de Estudios:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Institución:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel" style="display:none" id="labelOtra">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Otra Institución:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Correo:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Confirmación de Correo:</span></label>
                            </div>
                            <div class="row align-items-center justify-content-end customLabel">
                                <label class="form-control-label"><span style="color:black;font-weight:bold">Teléfono:</span></label>
                            </div>                            
                        </div><!--Etiquetas-->
                        <div class="col-12 col-md-9"><!--Campos-->
                            <div class="row row-sm">
                                <div class="form-group col-6">                                
                                    <select id="selectTipoParti" class="form-control" name="selectInst">
                                        <option disabled selected value="def">Elija una opción</option>
                                        @foreach ($tipo as $t)
                                            <option value="{{$t->tipo_parti}}">{{$t->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row row-sm" >                                 
                                <div class="col-md mg-t-20 mg-md-t-0"><!-- col_1 -->                          
                                  <input id="nomParti" class="form-control" name="nomParti" required>
                                  <label class="form-control-label" style="text-align:center">Nombre(s)</label>
                                </div>
                                <div class="col-md mg-t-20 mg-md-t-0"><!-- col_2 -->                          
                                  <input id="apParti" class="form-control" name="apParti" required>               
                                  <label class="form-control-label" style="text-align:center">Apellido Paterno</label>
                                </div>
                                <div class="col-md mg-t-20 mg-md-t-0"><!-- col_3 -->                          
                                  <input id="amParti" class="form-control" name="amParti" required>
                                  <label class="form-control-label" style="text-align:center">Apellido Materno</label>
                                </div>
                            </div> 
                            <div class="row row-sm"> <!--row_3-->                                                                  
                                <div class="form-group col-6">                                
                                    <select id="selectGrado" class="form-control" name="selectGrado">
                                        <option disabled selected value="def">Elija una opción</option>
                                        @foreach ($grado as $g)
                                            <option value="{{$g->id_grado}}">{{$g->nom_grado}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-8">                            
                                    <input id="grado" class="form-control" name="grado" style="display:none" required>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-6">                                
                                    <select id="selectPtl" class="form-control" name="selectPtl">
                                        <option disabled selected value="def">Elija una opción</option>
                                        <option value='ENP-UNAM'>ENP-UNAM</option>
                                        <option value='CCH-UNAM'>CCH-UNAM</option>
                                        <option value='otra'>OTRA</option>
                                        @foreach ($escuelas as $e)
                                            {{$cad = $e->ptl_ptl.' - '.$e->nombre}}
                                            <option value="{{$e->ptl_ptl}}">{{$cad}}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-8">                            
                                    <input id="plantel" class="form-control" name="plantel" style="display:none" required>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-8">                            
                                    <input id="correoPart" class="form-control" name="correoParti" type="email" required>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-8">                            
                                    <input id="confCorreoParti" class="form-control" name="confCorreoParti" type="email" required>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="form-group col-8">                            
                                    <input id="telParti" class="form-control" name="telParti" type="text" required>
                                </div>
                            </div>                       
                        </div><!--Campos-->
                    </div>
                    <br>
                    <div class="row"> <!--row_7-->
                        <div class="col-md-8 ml-md-auto">
                            <p>{{captcha_img()}}</p>
                        </div>                                                                      
                    </div> <!--row_7-->
                    <div class="row">
                        <div class="col-md-3 offset-md-4">
                            <input class="form-control" type="text" name="captcha">
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <a id="btnPreReg" class="btn btn-primary" style="color:#1b4f72;"><i data-feather="send" style="color:#1b4f72"></i> Enviar</a> 
                        </div>
                    </div>                    
                </form>
            </div><!-- card -->
        </div><!-- component-section -->
    {{-- </div> --}}
@endsection
@section('scripts')
    <script src="{{URL::asset('js/preRegistro.js')}}"></script>
@endsection