@extends('master')

@section('main')
<div align="center" style="padding-top:35px; padding-bottom:15px;">

<form id="formularioEnv" name="formularioEnv">
<table border="0" cellpadding="0" cellspacing="0" align="center" class="ui-corner-bottom ui-corner-top ui-widget-content">
	<tr><td class="ui-state-default ui-corner-top" colspan="2" style="height:30px; padding-left:5px;" align="center">Pre - Registro a congreso</td></tr>
    
    <tr><td style="height:30px" colspan="2"></td></tr>

	<tr><td colspan="2"><div class="ui-widget-header2 ui-corner-top ui-corner-bottom" align="center" style="padding:5px 0 5px 0; width:90%; margin:0 auto 0 auto; font-size:16px; margin-bottom:10px;">Datos generales</div></td></tr>
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr>
        <td class="td_label" width="150">Tipo de participante* : </td>
        <td align="left">
            <select name="slcTipoPart" id="slcTipoPart" class="ui-corner-all" data-bvalidator="required">
                @foreach ($tipo as $t)
                    <option value="{{$t->tipo_parti}}">{{$t->nombre}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td class="td_label" valign="top" width="130">Nombre* :</td>
	<td width="650">
		<table id="mi_estilo" cellpadding="0" cellspacing="0">
			<tr>
				<td><input type="text" size="25" maxlength="45" name="nombre" id="nombre" onchange="ucwords(this)" data-bvalidator="required"/></td>
                <td style="padding-left:5px; padding-right:5px;"><input type="text" size="25" maxlength="45" name="nom_pat" id="nom_pat" onchange="ucwords(this)" data-bvalidator="required"/></td>
				<td><input type="text" size="25" maxlength="45" name="nom_mat" id="nom_mat" onchange="ucwords(this)"/></td>
				
			</tr>
			<tr>
				<td class="minitext">Nombre(s)</td>
                <td class="minitext">Apellido Paterno</td>
				<td class="minitext">Apellido Materno</td>
			</tr>
		</table>
	</td></tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr>
        <td class="td_label" width="150">Grado de estudios* : </td>
        <td align="left">
            <select name="slcTipoGradoEst" id="slcTipoGradoEst" class="ui-corner-all" data-bvalidator="required">
                 @foreach ($grado as $g)
                    <option value="{{$g->id_grado}}">{{$g->nom_grado}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr style="display:none;" id="otraTipoGradoEst"><td class="td_label">Otro grado de estudios * : </td>
	<td align="left"><input type="text" size="70" maxlength="120" name="otraGradoEst" id="otraGradoEst" data-bvalidator="required"/></td></tr>
    
    <tr style="display:none;" id="separa2"><td style="height:10px" colspan="2"></td></tr>
    
    <tr>
        <td class="td_label" width="150">Instituci&oacute;n* : </td>
        <td align="left">
            <select name="slcPtl" id="slcPtl" class="ui-corner-all" data-bvalidator="required">
            @foreach ($escuelas as $e)
                         <option value="{{$e->ptl_ptl}}">{{utf8_encode($e->ptl_nombre)}}</option>
                     @endforeach         
            </select>
        </td>
    </tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr style="display:none;" id="otraPtl"><td class="td_label">Otra instituci&oacute;n* : </td>
	<td align="left"><input type="text" size="70" maxlength="120" name="otraInstitucion" id="otraInstitucion" data-bvalidator="required"/></td></tr>
    
    <tr style="display:none;" id="separa"><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td class="td_label">Correo electr&oacute;nico* : </td>
	<td align="left"><input type="text" size="70" maxlength="80" name="email" id="email" data-bvalidator="required, email,checkCorreoSol"/><span id="errorMail"></span></td></tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td class="td_label">Confirmaci&oacute;n de correo electr&oacute;nico* : </td>
	<td align="left"><input type="text" size="70" maxlength="80" name="email_c" id="email_c" data-bvalidator="equalto[email]"/></td></tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td class="td_label">Tel&eacute;fono* : </td>
	<td align="left"><input type="text" size="70" maxlength="15" name="telefono" id="telefono" data-bvalidator="required"/></td></tr>
   
   	<tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td style="height:10px" colspan="2"></td></tr>
    
    <tr><td colspan="2">
    <div id="errorContainer" align="left" style="width:70%; margin:0 auto 0 auto;">
        <p><b>&nbsp;Se detectaron los siguientes errores:</b></p>
        <ul></ul>
	</div>
    </td></tr>
    
    <tr><td style="height:30px" colspan="2" align="center"><button id="btnEnviar">Enviar</button></td></tr>
</table>
</form>
<button id="btnSalir" >Salir</button>
</div>
@endsection 