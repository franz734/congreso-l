<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <title>Congreso DGIRE</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link href="{{URL::asset('lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/css/cassie.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <!---->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/congreso.css') }}" />
    @yield('css')
</head>
<body>
    <div id="mainWrapper">
        <div id="headWrapper">
            <div id="headWrapper" class="ui-corner-top">
                <img src="../img/baner.png" height="102" border="0" usemap="#Map">
                <map name="Map" id="Map">
                    <area shape="rect" coords="20,5,249,99" href="http://congreso.dgire.unam.mx" target="_self" style="outline:none !important; "/>
                    <area shape="rect" coords="850,30,915,92" href="http://www.unam.mx" target="_self" style="outline:none !important; "/>
                    <area shape="rect" coords="930,28,970,92" href="http://www.dgire.unam.mx" target="_self" style="outline:none !important; "/>
                </map>
            </div>
        </div>
        
        <div id="contentWrapper">
            <div id="headLine"></div>
            @yield('contenido')
            <div id="footWrapper" class="ui-corner-bottom">
                <div id="headLine"></div>
                <br>
                Este sitio se ve mejor en los navegadores: <a href="http://www.mozilla-europe.org/es/firefox/">Firefox 3.5</a>,
                <a href="www.google.com/Chrome">Chrome</a> e
                <a href="http://www.microsoft.com/latam/windows/internet-explorer/">Internet Explorer 8.0</a> o superiores.<br />
                <b>Derechos reservados <a href="http://www.dgire.unam.mx">&copy; DGIRE {{date('Y')}}</a></b>.<br>
                <b>Versi&oacute;n Sistema 1.0</b>
            </div>
        </div>
        
    </div>
    <script src="{{URL::asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('lib/jqueryui/jquery-ui.min.js')}}"></script>
    <script src="{{URL::asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{URL::asset('lib/feather-icons/feather.min.js')}}"></script>
    <script src="{{URL::asset('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{URL::asset('lib/js-cookie/js.cookie.js')}}"></script>
    <script src="{{URL::asset('lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{URL::asset('lib/jquery.flot/jquery.flot.stack.js')}}"></script>
    <script src="{{URL::asset('lib/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{URL::asset('assets/js/cassie.js')}}"></script>
    <script src="{{URL::asset('assets/js/flot.sampledata.js')}}"></script>
    <script src="{{URL::asset('lib/prismjs/prism.js')}}"></script>
    <script src="{{URL::asset('lib/parsleyjs/parsley.min.js')}}"></script>
    <script src="{{URL::asset('lib/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <!---->
    <script type="text/javascript" src="{{ asset('/js/blockUI/jquery.blockUI.js') }}" ></script>
    @yield('scripts')
</body>
</html>