<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <title>@yield('titulo')</title>
    {{-- {!! htmlScriptTagJsApiV3([
        'action' => 'homepage',
        'callback_then' => 'callbackThen',
        'callback_catch' => 'callbackCatch'
        ]) !!} --}}
    @yield('head')
</head>
<body>

    <div id="mainWrapper">
        <div id="headWrapper">
            <div id="headWrapper" class="ui-corner-top">
                <img src="../img/baner.png" height="102" border="0" usemap="#Map">
                <map name="Map" id="Map">
                    <area shape="rect" coords="20,5,249,99" href="http://congreso.dgire.unam.mx" target="_self" style="outline:none !important; "/>
                    <area shape="rect" coords="850,30,915,92" href="http://www.unam.mx" target="_self" style="outline:none !important; "/>
                    <area shape="rect" coords="930,28,970,92" href="http://www.dgire.unam.mx" target="_self" style="outline:none !important; "/>
                </map>
            </div>
        </div>
        
        <div id="contentWrapper">
            <div id="headLine"></div>
            @yield('main')
            <div id="footWrapper" class="ui-corner-bottom">
                <div id="headLine"></div>
                <br>
                Este sitio se ve mejor en los navegadores: <a href="http://www.mozilla-europe.org/es/firefox/">Firefox 3.5</a>,
                <a href="www.google.com/Chrome">Chrome</a> e
                <a href="http://www.microsoft.com/latam/windows/internet-explorer/">Internet Explorer 8.0</a> o superiores.<br />
                <b>Derechos reservados <a href="http://www.dgire.unam.mx">&copy; DGIRE {{date('Y')}}</a></b>.<br>
                <b>Versi&oacute;n Sistema 1.0</b>
            </div>
        </div>
        
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.validate.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('/js/messages_es.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('/js/blockUI/jquery.blockUI.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('/js/numeric/jquery.numeric.js') }}" ></script>
    <script>
        function callbackThen(response){
            // read HTTP status
            console.log(response.status);
            
            // read Promise object
            response.json().then(function(data){
                console.log(data);
            });
        }
        function callbackCatch(error){
            console.error('Error:', error)
        } 
    </script>
    @yield('scripts')
    

</body>
</html>